import {get, param} from '@loopback/rest';
import * as fs from 'fs';
const axios = require('axios');
import { callbackify } from 'util';
const clone = require('git-clone/promise');




export class HelloController {

    savefile(data: object, method: string) {
        // wil je result UIT data; data.result ==> oftewel result uit data (achter naar voor)
        // toString() op een object is "Object object", door middel van JSON.stringify kunnen we er een mooie tekst van maken   

        var text = JSON.stringify(data, null, 3);
        
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        
        //Dit maakt de map aan 
        var dir = './backup';
        
        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir); 
        }
    
        //Dit maakt de txt file aan
        fs.writeFile('./backup/output-'+ method + "-" + dd + "-" + mm + "-" + yyyy + '.txt', text, function (err) {
                if (err) return console.log(err);
                console.log('resultaat.txt opgeslagen in backup');
            }
        )
    }


    @get('/api') // de slash hier is je url
    async api(): Promise<any> {
        // axios (https://nodejs.dev/learn/making-http-requests-with-nodejs) is een HTTP Request plugin die er voor zorgt dat we GET requests uit kunnen voeren
        // de await (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/await) methode zorgt ervoor dat we pas verder gaan als het resultaat uit axios.get bekend is.
        // Een get is het zelfde als via de browser naar een pagina gaan. (GET == ophalen)
        const result = await axios.get("https://api.dev.whysor.com");
        // In data zit het resultaat van de GET request.
        return result.data;
    }


    @get('/apigit')
    async apigit(): Promise<any> {
        try {
            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();

            //await this.git.addRemote("hellogitworld", "https://gitlab.com/gitlab-examples/docker.git");

            //Dit maakt de map aan 
            var dir1 = './backup/';
            var dir2 = dir1 + dd + "-" + mm + "-" + yyyy + "/";
            var dir3 = dir2 + "hellogitworld" + '/'


            if (!fs.existsSync(dir1)) {
                fs.mkdirSync(dir1); 
            }

            if (!fs.existsSync(dir2)) {
                fs.mkdirSync(dir2); 
            }

            if (!fs.existsSync(dir3)) {
                fs.mkdirSync(dir3); 
            }

            // try catch en melding als map al bestaat
            // await this.git.clone("https://github.com/githubtraining/hellogitworld", dir3);
        } catch (error) {
            console.error(error)
        }
  }


  @get('/apigitlab')
    async apigitlab(): Promise<any> {
        try {
            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();
            //await this.git.addRemote("hellogitworld", "https://gitlab.com/gitlab-examples/docker.git");

            //Dit maakt de map aan 
            var dir1 = './backup/';
            var dir2 = dir1 + dd + "-" + mm + "-" + yyyy + "/";
            var dir3 = dir2 + "backupsysteem" + '/'
            var dir4 = dir2 + "backupsysteem branch" + '/'


            if (!fs.existsSync(dir1)) {
                fs.mkdirSync(dir1); 
            }

            if (!fs.existsSync(dir2)) {
                fs.mkdirSync(dir2); 
            }

            if (!fs.existsSync(dir3)) {
                fs.mkdirSync(dir3); 
            }

            if (!fs.existsSync(dir4)) {
                fs.mkdirSync(dir4); 
            }


            await clone("https://gitlab.com/ties125808/backup-systeem", dir3);
            await clone("https://gitlab.com/ties125808/backup-systeem", dir4, {checkout: "branchnummertwee"});
        } catch (error) {
            console.error(error)
        }
  }



    @get('/whysortrello')
    async apiwhysor(): Promise<any> {

        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0'); //Padstart maakt de 0 in de '01', '02', etc. Voor sortering.
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        //'Await' is dat de code wacht tot er op een antwoord word gegeven.
        const result = await axios.get("https://api.trello.com/1/boards/Jds2btxJ?key=df7b94df1893fe82548b89732727d8b2&token=dfef12d721c3995043ea1257f8da8e71445e24404c27884960b60571244ff6b2&cards=all&members=all&lists=all&labels=all&customFields=true&checklists=all");            
        const result2 = await axios.get("https://api.trello.com/1/boards/C2Y5jVCC?key=df7b94df1893fe82548b89732727d8b2&token=dfef12d721c3995043ea1257f8da8e71445e24404c27884960b60571244ff6b2&cards=all&members=all&lists=all&labels=all&customFields=true&checklists=all");       
        const result3 = await axios.get("https://api.trello.com/1/boards/bOmS30Ps?key=df7b94df1893fe82548b89732727d8b2&token=dfef12d721c3995043ea1257f8da8e71445e24404c27884960b60571244ff6b2&cards=all&members=all&lists=all&labels=all&customFields=true&checklists=all");       
        const result4 = await axios.get("https://api.trello.com/1/boards/pMLhtmiV?key=df7b94df1893fe82548b89732727d8b2&token=dfef12d721c3995043ea1257f8da8e71445e24404c27884960b60571244ff6b2&cards=all&members=all&lists=all&labels=all&customFields=true&checklists=all");       
        const result5 = await axios.get("https://api.trello.com/1/boards/WBTe4NL5?key=df7b94df1893fe82548b89732727d8b2&token=dfef12d721c3995043ea1257f8da8e71445e24404c27884960b60571244ff6b2&cards=all&members=all&lists=all&labels=all&customFields=true&checklists=all");       
        const result6 = await axios.get("https://api.trello.com/1/boards/aM04yfEP?key=df7b94df1893fe82548b89732727d8b2&token=dfef12d721c3995043ea1257f8da8e71445e24404c27884960b60571244ff6b2&cards=all&members=all&lists=all&labels=all&customFields=true&checklists=all");      
        const result7 = await axios.get("https://api.trello.com/1/boards/VNRIaJKG?key=df7b94df1893fe82548b89732727d8b2&token=dfef12d721c3995043ea1257f8da8e71445e24404c27884960b60571244ff6b2&cards=all&members=all&lists=all&labels=all&customFields=true&checklists=all");      
        const result8 = await axios.get("https://api.trello.com/1/boards/nYN5Rul4?key=df7b94df1893fe82548b89732727d8b2&token=dfef12d721c3995043ea1257f8da8e71445e24404c27884960b60571244ff6b2&cards=all&members=all&lists=all&labels=all&customFields=true&checklists=all");      

        //console.log(result.data.name);
        
    
        //Dit maakt de map aan 
        var dir1 = './backup/';
        var dir2 = dir1 + dd + "-" + mm + "-" + yyyy + "/";
        
        //var dir3 = dir2 + result.data.name + '/'
        
        var dir3 = dir2 + result.data.name.replace(":", "") + '/'
        var dir4 = dir2 + result2.data.name.replace(":", "") + '/'
        var dir5 = dir2 + result3.data.name.replace(":", "") + '/'
        var dir6 = dir2 + result4.data.name.replace(":", "") + '/'
        var dir7 = dir2 + result5.data.name.replace(":", "") + '/'
        var dir8 = dir2 + result6.data.name.replace(":", "") + '/'
        var dir9 = dir2 + result7.data.name.replace(":", "") + '/'
        var dir10 = dir2 + result8.data.name.replace(":", "") + '/'


        //console.log(result);
        //Stringify slaate JSON file op in text
        var text = JSON.stringify(result.data, null, 3);
        var text2 = JSON.stringify(result2.data, null, 3);
        var text3 = JSON.stringify(result3.data, null, 3);
        var text4 = JSON.stringify(result4.data, null, 3);
        var text5 = JSON.stringify(result5.data, null, 3);
        var text6 = JSON.stringify(result6.data, null, 3);
        var text7 = JSON.stringify(result7.data, null, 3);
        var text8 = JSON.stringify(result8.data, null, 3);
        

        if (!fs.existsSync(dir1)) {
            fs.mkdirSync(dir1); 
        }

        if (!fs.existsSync(dir2)) {
            fs.mkdirSync(dir2); 
        }

        if (!fs.existsSync(dir3)) {
            fs.mkdirSync(dir3); 
        }

        if (!fs.existsSync(dir4)) {
            fs.mkdirSync(dir4); 
        }

        if (!fs.existsSync(dir5)) {
            fs.mkdirSync(dir5); 
        }
    
        if (!fs.existsSync(dir6)) {
            fs.mkdirSync(dir6); 
        }

        if (!fs.existsSync(dir7)) {
            fs.mkdirSync(dir7); 
        }

        if (!fs.existsSync(dir8)) {
            fs.mkdirSync(dir8); 
        }
    
        if (!fs.existsSync(dir9)) {
            fs.mkdirSync(dir9); 
        }
        
        if (!fs.existsSync(dir10)) {
            fs.mkdirSync(dir10); 
        }

        //Dit maakt de txt file aan
        fs.writeFile(dir3 + 'ontwikkelingactie.json', text, function (err) {
            if (err) return console.log(err);
            console.log('ontwikkelingactie.json opgeslagen in backup');
        });

        //Dit maakt de txt file aan
        fs.writeFile(dir4 + 'ontwikkelingscrum.json', text2, function (err) {
            if (err) return console.log(err);
            console.log('ontwikkelingscrum.json opgeslagen in backup');
        });

        //Dit maakt de txt file aan
        fs.writeFile(dir5 + 'ontwikkelingplanning.json', text3, function (err) {
            if (err) return console.log(err);
            console.log('ontwikkelingplanning.json opgeslagen in backup');
        });
        
        //Dit maakt de txt file aan
        fs.writeFile(dir6 + 'ontwikkelingscrumarchief.json', text4, function (err) {
            if (err) return console.log(err);
            console.log('ontwikkelingscrumarchief.json opgeslagen in backup');
        });
        
        //Dit maakt de txt file aan
        fs.writeFile(dir7 + 'datawarehouse.json', text5, function (err) {
            if (err) return console.log(err);
            console.log('datawarehouse.json opgeslagen in backup');
        });

        //Dit maakt de txt file aan
        fs.writeFile(dir8 + 'vlinderstichting.json', text6, function (err) {
            if (err) return console.log(err);
            console.log('vlinderstichting.json opgeslagen in backup');
        });        

        //Dit maakt de txt file aan
        fs.writeFile(dir9 + 'tweetingtree.json', text7, function (err) {
            if (err) return console.log(err);
            console.log('tweetingtree.json opgeslagen in backup');
        });        

        //Dit maakt de txt file aan
        fs.writeFile(dir10 + 'ties.json', text8, function (err) {
            if (err) return console.log(err);
            console.log('ties.json opgeslagen in backup');
        });      
        
        return result.data;
    }


    @get('/apitrelloboard')
    async api3(): Promise<any> {

        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        const result = await axios.get("https://api.trello.com/1/boards/nYN5Rul4?key=5f42ba49cd2696add1e3f232652ec6d4&token=f8f534e6258c3fd063f1e425d8c2bca254122d76d3f818812fa64b131e417822&cards=all&members=all&lists=all&labels=all&customFields=true&checklists=all");       
        const result2 = await axios.get("https://api.trello.com/1/boards/weLMbuwB?key=5f42ba49cd2696add1e3f232652ec6d4&token=f8f534e6258c3fd063f1e425d8c2bca254122d76d3f818812fa64b131e417822&cards=all&members=all&lists=all&labels=all&customFields=true&checklists=all");       


        //console.log(result.data.name);
        
    
        //Dit maakt de map aan 
        var dir1 = './backup/';
        var dir2 = dir1 + dd + "-" + mm + "-" + yyyy + "/";
        var dir3 = dir2 + result.data.name + '/'
        var dir4 = dir2 + result2.data.name + '/'
        //console.log(result);
        //console.log(result2);
        var text = JSON.stringify(result.data, null, 3);
        var text2 = JSON.stringify(result2.data, null, 3);


        if (!fs.existsSync(dir1)) {
            fs.mkdirSync(dir1); 
        }

        if (!fs.existsSync(dir2)) {
            fs.mkdirSync(dir2); 
        }

        if (!fs.existsSync(dir3)) {
            fs.mkdirSync(dir3); 
        }

        if (!fs.existsSync(dir4)) {
            fs.mkdirSync(dir4); 
        }
    
        //Dit maakt de txt file aan
        fs.writeFile(dir3 + 'trello.txt', text, function (err) {
            if (err) return console.log(err);
            console.log('trello.txt opgeslagen in backup');
        });

        //Dit maakt de txt file aan
        fs.writeFile(dir4 + 'trello.txt', text2, function (err) {
            if (err) return console.log(err);
            console.log('trello.txt opgeslagen in backup');
        });
        return result.data;
    }


    @get('/apiwhysor')
    async api5(): Promise<any> {

        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        const result = await axios.get("https://api.dev.whysor.com/publicdashboards/1afa5a4b-a863-4fef-a854-196fbc1c0512");       
        //Dit maakt de map aan 
        var dir = './backup/'+ dd + "-" + mm + "-" + yyyy;
        console.log(result);
        var text = JSON.stringify(result.data, null, 3);


        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir); 
        }
    
        //Dit maakt de txt file aan
        fs.writeFile(dir + '/whysor.txt', text, function (err) {
                if (err) return console.log(err);
                console.log('whysor.txt opgeslagen in backup');
            }
        )
        return result.data;
    }
    




    @get('/add/{parameter}/{parameter2}') // de slash hier is je url
    add(
        @param.path.number('parameter') parameter: number, // dit zijn je parameters
        @param.path.number('parameter2') parameter2: number,
    ): object {
        // Object met resultaat
        var data = {
            message: 'Calculator API - made by Ties',
            result: parameter + parameter2
        };

        this.savefile(data, 'add')
                
        return data;
    }



    @get('/sub/{parameter}/{parameter2}')
    subtract(
        @param.path.number('parameter') parameter: number,
        @param.path.number('parameter2') parameter2: number,
    ): object {
        var data = {
            message: 'Calculator API - made by Ties',
            result: parameter - parameter2
        };
           
        this.savefile(data, 'sub')

        return data;
    }



    @get('/mul/{parameter}/{parameter2}')
    multiply(
        @param.path.number('parameter') parameter: number,
        @param.path.number('parameter2') parameter2: number,
    ): object {
        var data = {
            message: 'Calculator API - made by Ties',
            result: parameter * parameter2
        };

        this.savefile(data, 'mul')

        return data;
    }



    @get('/div/{parameter}/{parameter2}')
    divide(
        @param.path.number('parameter') parameter: number,
        @param.path.number('parameter2') parameter2: number,
    ): object {
        var data = {
            message: 'Calculator API - made by Ties',
            result: parameter / parameter2,     
        };

        this.savefile(data, 'div')

        return data;
    }



    @get('/pow/{parameter}/{parameter2}')
    power(
        @param.path.number('parameter') parameter: number,
        @param.path.number('parameter2') parameter2: number,
    ): object {
        var data = {
            message: 'Calculator API - made by Ties',
            result: Math.pow(parameter, parameter2)
        };

        this.savefile(data, 'pow')

        return data;
    }


}





//https://loopback.io/doc/en/lb4/index.html


// //Start server
// const init = async () => {
//     await server.start();
//     console.log("Server up test! Port: " + port);
// }

//     //route - about
//     server.route({
//         method: 'GET',
//         path: '/rekenmachine',
//         handler: function (request: any, h: any) {

//             var data = {
//                 message: 'Calculator API - made by Ties'
//             };

//             return data;
//         }
//     });

//     //route - sum
//     server.route({
//         method: 'GET',

//         path: '/calculator/sum/{num1}+{num2}',
//         handler: function (request: { params: { num1: string; num2: string; }; }, h: any) {

//             const num1 = parseInt(request.params.num1);
//             const num2 = parseInt(request.params.num2);

//             var data = {
//                 answer: num1 + num2
//             };

//             return data;
//         }
//     });

//     //route - subtraction
//     server.route({
//         method: 'GET',

//         path: '/calculator/sub/{num1}-{num2}',
//         handler: function (request: { params: { num1: string; num2: string; }; }, h: any) {

//             const num1 = parseInt(request.params.num1);
//             const num2 = parseInt(request.params.num2);

//             var data = {
//                 answer: num1 - num2
//             };

//             return data;
//         }
//     });

//     //route - multiplication
//     server.route({
//         method: 'GET',

//         path: '/calculator/multi/{num1}*{num2}',
//         handler: function (request: { params: { num1: string; num2: string; }; }, h: any) {

//             const num1 = parseInt(request.params.num1);
//             const num2 = parseInt(request.params.num2);

//             var data = {
//                 answer: num1 * num2
//             };

//             return data;
//         }
//     });

//     //route - division
//     server.route({
//         method: 'GET',

//         path: '/calculator/div/{num1}/{num2}',
//         handler: function (request: { params: { num1: string; num2: string; }; }, h: any) {

//             const num1 = parseInt(request.params.num1);
//             const num2 = parseInt(request.params.num2);

//             var data = {
//                 answer: num1 / num2
//             };

//             return data;
//         }
//     });
// //Start App
// init();